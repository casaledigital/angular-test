import { Component, OnInit, ViewChild } from '@angular/core';
import {
  MatDrawerMode,
  MatSidenav,
  MatSidenavModule,
} from '@angular/material/sidenav';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Router, Event, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit {
  @ViewChild('drawer', { static: false })
  drawer!: MatSidenav;
  sidebarType!: MatDrawerMode;
  sidebarStatus = false;
  isMobile = false;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router
  ) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart && this.isMobile) {
        if (this.drawer.opened) {
          this.drawer.toggle();
        }
      }
    });
    this.breakpointObserver
      .observe([Breakpoints.XSmall, Breakpoints.Small])
      .subscribe((result) => {
        this.sidebarType = result.matches ? 'push' : 'side';
        //this.sidebarStatus = !result.matches;
        this.isMobile = result.matches;
      });
  }

  ngOnInit(): void {}

  login(): void {}
  logout(): void {}
}
