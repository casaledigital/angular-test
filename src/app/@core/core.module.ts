import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { HomeLayoutComponent } from './layout/home-layout/home-layout.component';

@NgModule({
  declarations: [ MainLayoutComponent, HomeLayoutComponent],
  imports: [CommonModule, SharedModule, AppRoutingModule],
})
export class CoreModule {}
