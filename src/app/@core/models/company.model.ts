export interface Company {
  id: number;
  name: string;
  addresse: string;
  country: string;
}
