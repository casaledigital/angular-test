export interface User {
  id?: number;
  first_name: string;
  last_name: string;
  gender: Gender;
  email: string;
  company: string;
}

export enum Gender {
  Male = 'Male',
  Female = 'Female',
}

export enum EditMode {
  Create = 'Create',
  Update = 'Update',
}
