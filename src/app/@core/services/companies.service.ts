import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Company } from '../models';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root',
})
export class CompaniesService {
  constructor(private globalService: GlobalService, private http: HttpClient) {}

  /**
   *   http api call methods to companies endpoint
   */

  getAllCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(`${this.globalService.apiHost}companies`);
  }

  getCompany(id: number): Observable<Company> {
    return this.http.get<Company>(
      `${this.globalService.apiHost}companies/${id}`
    );
  }
}
