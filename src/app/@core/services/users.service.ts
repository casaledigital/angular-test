import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { User } from '../models';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private usersList = new BehaviorSubject<User[] | null>(null);
  private selectedUser = new BehaviorSubject<User | null>(null);
  usersList$!: Observable<User[] | null>;
  selectedUser$!: Observable<User | null>;

  constructor(private globalService: GlobalService, private http: HttpClient) {
    this.usersList$ = this.usersList.asObservable();
    this.selectedUser$ = this.selectedUser.asObservable();
  }

  /**
   * methods that handle usersList and selectedUser BehaviorSubjects
   */

  fetchUsersList(users: User[]): void {
    this.usersList.next(users);
  }

  setSelectedUser(user: User): void {
    this.selectedUser.next(user);
  }

  resetSelectedUser(): void {
    this.selectedUser.next(null);
  }

  addUserInlist(user: User): void {
    const users = this.usersList.getValue() || [];
    const updatedUsers = [...users, user];

    this.usersList.next(updatedUsers);
  }

  deleteUserInlist(id: number): void {
    const users = this.usersList.getValue() || [];
    const updatedUsers = users.filter((user) => user.id !== id);

    this.usersList.next(updatedUsers);

    if (
      this.selectedUser.getValue() &&
      this.selectedUser.getValue()?.id === id
    ) {
      this.resetSelectedUser();
    }
  }

  /**
   *   http api call methods to users endpoint
   */

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.globalService.apiHost}users`);
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(`${this.globalService.apiHost}users/${id}`);
  }

  createUser(userDto: User): Observable<User> {
    return this.http.post<User>(`${this.globalService.apiHost}users`, userDto);
  }

  updateUser(userDto: User): Observable<User> {
    return this.http.put<User>(
      `${this.globalService.apiHost}users/${userDto.id}`,
      userDto
    );
  }

  deleteUser(id: number): Observable<void> {
    return this.http.delete<void>(`${this.globalService.apiHost}users/${id}`);
  }
}
