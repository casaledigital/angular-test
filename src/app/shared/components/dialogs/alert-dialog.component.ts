import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-alert-dialog',
  template: `
    <mat-dialog-content>
      <h1>{{ alertMessage.title }}</h1>
      <p [innerHtml]="showMessage"></p>
    </mat-dialog-content>
    <mat-dialog-actions align="end">
      <button mat-raised-button (click)="dialogRef.close()">CLOSE</button>
    </mat-dialog-actions>
  `
})
export class AlertDialogComponent {
  showMessage: SafeHtml;
  constructor(
    private sanitizer: DomSanitizer,
    public dialogRef: MatDialogRef<AlertDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public alertMessage: {
      type: string;
      title: string;
      message: string;
    }
  ) {
    this.showMessage = this.sanitizer.bypassSecurityTrustHtml(
      this.alertMessage.message
    );
  }
}
