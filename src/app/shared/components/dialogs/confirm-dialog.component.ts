import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-confirm-dialog',
  template: `
    <mat-dialog-content>
      <h1>Confirm Action</h1>
      <p [innerHtml]="showMessage"></p>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-raised-button (click)="dialogRef.close(true)" color="primary">
        YES
      </button>
      <button mat-raised-button (click)="dialogRef.close(false)">NO</button>
    </mat-dialog-actions>
  `
})
export class ConfirmDialogComponent {
  showMessage: SafeHtml;
  constructor(
    private sanitizer: DomSanitizer,
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public confirmMessage: string
  ) {
    this.showMessage = this.sanitizer.bypassSecurityTrustHtml(
      this.confirmMessage
    );
  }
}
