import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './modules/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { ErrorComponent } from './components/error/error.component';
import { AlertDialogComponent } from './components/dialogs/alert-dialog.component';
import { ConfirmDialogComponent } from './components/dialogs/confirm-dialog.component';
import { LoadingComponent } from './components/loading/loading.component';

@NgModule({
  declarations: [
    LoadingComponent,
    AlertDialogComponent,
    ConfirmDialogComponent,
    ErrorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    LoadingComponent,
    ErrorComponent
  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  entryComponents: [AlertDialogComponent, ConfirmDialogComponent],
})
export class SharedModule {}
