import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import localeItCH from '@angular/common/locales/it-CH';
import localeItCHExtra from '@angular/common/locales/extra/it-CH';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { registerLocaleData } from '@angular/common';
import { CoreModule } from './@core/core.module';
import { SharedModule } from './shared/shared.module';

registerLocaleData(localeItCH, 'it-CH', localeItCHExtra);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'it-CH',
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
