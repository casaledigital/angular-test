import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { EditMode, User } from 'src/app/@core/models';
import { GlobalService, UsersService } from 'src/app/@core/services';
import * as moment from 'moment';
import { AlertDialogComponent } from 'src/app/shared/components/dialogs/alert-dialog.component';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit {
  userForm!: FormGroup;
  constructor(
    private fb: FormBuilder,
    private usersService: UsersService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<UserFormComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      user: User;
      editMode: EditMode;
    }
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  closeDialog(result: User | null = null) {
    this.dialogRef.close(result);
  }

  save(): void {
    const newUser = this.userForm.value;
    this.usersService.createUser(newUser).subscribe(
      (user: User) => {
        this.closeDialog(user);
      },
      (error: any) => {
        this.dialog.open(AlertDialogComponent, {
          data: {
            type: 'error',
            title: 'Pay Attention!',
            message: 'An error occurred adding item. Please try again.',
          },
        });
        console.log(error);
      }
    );
  }

  private initForm() {
    this.userForm = this.fb.group({
      first_name: [null, Validators.required],
      last_name: [null, Validators.required],
      gender: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      company: [null],
    });
  }
}
