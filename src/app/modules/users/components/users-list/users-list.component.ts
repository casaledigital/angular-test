import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/@core/models';
import { ConfirmDialogComponent } from 'src/app/shared/components/dialogs/confirm-dialog.component';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements OnInit {
  columns!: string[];
  displayedColumns!: string[];
  dataSource!: MatTableDataSource<any>;
  private paginator!: MatPaginator;
  private sort!: MatSort;
  noDataMessage = 'No data available';
  @Input() set data(val: User[] | null) {
    if (val) {
      this.dataSource.data = val;
    }
  }
  @Output() delete = new EventEmitter<number>();

  @ViewChild(MatPaginator, { static: false }) set matPaginator(
    mp: MatPaginator
  ) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }
  @ViewChild(MatSort, { static: false }) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }
  constructor(private dialog: MatDialog) {
    this.columns = [
      'id',
      'first_name',
      'last_name',
      'gender',
      'email',
      'company',
    ];
    this.displayedColumns = [
      'first_name',
      'last_name',
      'gender',
      'email',
      'company',
      'actions',
    ];
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit(): void {}

  deleteUser(user: User): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: `Are you sure you want delete this user?`,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        // if user click Yes -> return true , No -> false
        this.delete.emit(user.id);
      }
    });
  }

  private setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
