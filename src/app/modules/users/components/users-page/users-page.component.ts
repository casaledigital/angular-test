import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { EditMode, User } from 'src/app/@core/models';
import { GlobalService, UsersService } from 'src/app/@core/services';
import { AlertDialogComponent } from 'src/app/shared/components/dialogs/alert-dialog.component';
import { UserFormComponent } from '../user-form/user-form.component';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss'],
})
export class UsersPageComponent implements OnInit {
  loadingData = true;
  users$: Observable<User[] | null>;
  constructor(
    private usersService: UsersService,
    private globalService: GlobalService,
    private dialog: MatDialog
  ) {
    this.users$ = this.usersService.usersList$;
  }

  ngOnInit(): void {
    this.getAllUsers();
  }

  getAllUsers(): void {
    this.usersService.getAllUsers().subscribe(
      (users: User[]) => {
        this.loadingData = false;
        this.usersService.fetchUsersList(users);
      },
      (error: any) => {
        this.loadingData = false;
        this.dialog.open(AlertDialogComponent, {
          data: {
            type: 'error',
            title: 'Pay Attention!',
            message: 'An error occurred retrieving data. Please try again.',
          },
        });
        console.log(error);
      }
    );
  }

  onDeleteUser(id: number): void {
    this.usersService.deleteUser(id).subscribe(
      () => {
        this.globalService.showSuccessMessage('Item deleted successfully!');
        this.usersService.deleteUserInlist(id);
      },
      (error: any) => {
        this.dialog.open(AlertDialogComponent, {
          data: {
            type: 'error',
            title: 'Pay Attention!',
            message: 'An error occurred deleting item. Please try again.',
          },
        });
        console.log(error);
      }
    );
  }

  onAddUser(): void {
    const dialogRef = this.dialog.open(UserFormComponent, {
      data: { user: null, editMode: EditMode.Create },
      minWidth: '320px',
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        // if user is updated successfully result contains updated user
        this.usersService.addUserInlist(result);
        this.globalService.showSuccessMessage('New user added successfully!');
      }
    });
  }
}
